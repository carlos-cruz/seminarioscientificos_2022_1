package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {
    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;
    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        professor.adicionarSeminario(this);
        areaCientifica.adicionarSeminario(this);
        this.areasCientificas.add(areaCientifica);
        this.professores.add(professor);
        this.qtdInscricoes = qtdInscricoes;
        criarInscricoes();
    }

    private void criarInscricoes() {
        for (int i = 0; i < qtdInscricoes; i++) {
            inscricoes.add(new Inscricao(this));
        }
    }

    public Seminario() {};

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        areaCientifica.adicionarSeminario(this);
        areasCientificas.add(areaCientifica);
    }

    public boolean possuiAreaCientifica(AreaCientifica areaCientifica) {
        return areasCientificas.contains(areaCientifica);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        inscricao.setSeminario(this);
        inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return inscricoes.contains(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        professor.adicionarSeminario(this);
        professores.add(professor);
    }

    public boolean possuiProfessor(Professor professor) {
        return professores.contains(professor);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public List<Professor> getProfessores() {
        return professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public List<Inscricao> getInscricoes() {
        return inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }
}
