package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

import java.time.LocalDateTime;

public class Inscricao {
    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        dataCriacao = LocalDateTime.now();
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        dataCompra = LocalDateTime.now();
        situacao = SituacaoInscricaoEnum.COMPRADO;
        estudante.adicionarInscricao(this);
    }

    public void cancelarCompra() {
        estudante.removerInscricao(this);
        estudante = null;
        direitoMaterial = null;
        dataCompra = null;
        situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void realizarCheckIn() {
        dataCheckIn = LocalDateTime.now();
        situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

    public Seminario getSeminario() {
        return seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }
}
